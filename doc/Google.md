# GoogleDrive

- [GoogleDrive](#googledrive)
  - [Methods](#methods)
    - [__construct](#__construct)
    - [getCloudFile](#getcloudfile)
    - [getContent](#getcontent)
    - [saveAs](#saveas)

## Methods

### __construct
使用Google API 金鑰建立`GoogleDriveClient`

- Description
  - __construct(string `$serviceAccountPath` = null)
- Parameters
  - $serviceAccountPath (*string*) – 金鑰檔案位置
- Returns
  - *object* - $this
- Example
  
  ```php
  use \DpMIS\Ci4extension\Libraries\Google\GoogleDrive;

  $drive = new GoogleDrive(APPPATH.'config/GoogleDriveApiToken.json');
  ```

### getCloudFile
使用`Google Drive File ID`獲取檔案暫存於記憶體內
- Description
  - getCloudFile(string `$fileId`, string `$mimeType` = null): string
- Parameters
  - $fileId (*string*) – 雲端檔案ID(需要授予金鑰對應帳號有讀取權限)
  - $mimeType (*string*) - 請參照`GoogleMIME.php`做設定
- Returns
  - *object* - $this
- Example
  ```php
  $drive->getCloudFile('your-sheet-file-id',"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet");
  ```

### getContent
獲取快取在記憶體內的檔案內容

- Description
  - getContent(): string
- Returns
  - *string* - $fileContent
- Example
  ```php
  $content = $drive->getCloudFile('your-sheet-file-id',"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")->getContent();
  ```

### saveAs
將快取在記憶體內的檔案存成實體檔案
- Description
  - saveAs(string $filePath): bool
- Parameters
  - $fileId (*string*) – 雲端檔案ID(需要授予金鑰對應帳號有讀取權限)
  - $mimeType (*string*) - 請參照`GoogleMIME.php`做設定
- Returns
  - *object* - $this
- Example
  ```php
  $savePath = APPPATH . "writable/GoogleDrive/Example.xlsx";

  $drive->getCloudFile('your-sheet-file-id',"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")->saveAs(savePath);
  ```