<?php

namespace DpMIS\Ci4extension\Models;

/**
 * Model Class
 */
abstract class DP_BaseModel extends \CodeIgniter\Model
{
    // Default Database
    // 注意!一定要記得設定他!!!
    protected $DBGroup = null;

    // Default select col
    // $allowedFields 若設定為 * ，會在建構子自動初始化
    protected array|string $DEFAULT_COL = '*';
    protected $allowedFields = '*';

    // Default Table name
    // $table 若設定為空，會在建構子自動初始化為小寫的class name
    protected $table = '';

    // Default primary key
    protected $primaryKey = 's_sn';
    protected $useAutoIncrement = true;

    // Default return type
    protected $returnType     = 'array';

    // Default get Rec_status='A'
    protected $getEnableData = true;

    // Create / Update Time
    protected $useTimestamps = true;
    protected $dateFormat = 'datetime';
    protected $createdField  = 'c_time';
    protected $updatedField  = 'u_time';

    // Delete Time(CI4 soft delete is using d_time field)
    protected $useSoftDeletes = true;
    protected $deletedField  = 'd_time';

    // Create / Update / Delete User
    protected $c_userField  = 'c_user';
    protected $u_userField  = 'u_user';
    protected $d_userField  = 'd_user';

    // 定義 新增/查詢/修改/刪除 額外執行函式
    protected $beforeInsert = ['set_c_user'];
    protected $beforeUpdate = ['set_u_user'];
    protected $beforeFind = ['addsql_rec_status'];
    protected $afterFind = [];
    protected $beforeDelete = ['get_deleting_ids'];
    protected $afterDelete = ['set_rec_status'];

    /**
     * 需要實作getUserId，供填寫c_user/u_user/d_user使用
     * 注意要考慮cli/api及WebUI等狀況
     *
     * @return string|int
     */
    abstract protected function getUserId();

    // 定義rec_status true的值
    protected $recStatusField = 'rec_status';
    protected $recStatusTrueValue = 'A';
    protected $recStatusFalseValue = 'R';

    protected $deleting_ids = [];

    /**
     * Class constructor
     *
     * @return	void
     */
    public function __construct()
    {
        // 如果沒有設定 table name
        // 自動初始化 table name
        if ($this->table == '') {
            $table = strtolower(get_class($this));
            $table = explode('\\', $table);
            $this->table = end($table);
        }

        // 如果允許的欄位為 * ，自動初始化為所有欄位
        if ($this->allowedFields == '*') {
            $db = \Config\Database::connect($this->DBGroup);
            $this->allowedFields = $db->getFieldNames($this->table);
        }

        // 初始化父類別
        parent::__construct();
    }

    // --------------------------------------------------------------------

    /**
     * 取得Table Name
     *
     * @return \string
     */
    public function getTable()
    {
        return $this->table;
    }

    /**
     * Select函式，支援輸入陣列或字串
     * EX:
     *      $col = 's_sn,name'
     *      $col = ['s_sn','name']
     *      $col = '' => 使用 $this->DEFAULT_COL
     *
     * @param string|array $col
     * @return object $this
     */
    public function select($col = '')
    {
        if (is_string($col)) {
            // 輸入字串
            if ($col == '') {
                if (is_array($this->DEFAULT_COL)) {
                    $col = implode(',', $this->DEFAULT_COL);
                } else {
                    $col = $this->DEFAULT_COL;
                }
            }
        } elseif (is_array($col)) {
            // 輸入陣列
            if ($col == []) {
                $col = $this->DEFAULT_COL;
            } else {
                $col = implode(',', $col);
            }
        } else {
            // 輸入其他
            $col = $this->DEFAULT_COL;
        }

        // 設定選擇
        $this->builder()->select($col);

        return $this;
    }

    /**
     * Where 函式綜合體
     * @var array $where = [] 以Key => Value 形式組合的陣列
     * 
     * ex:
     * $where = [
     *      'name' => ['Alice','Bob'],
     *      'age !='  => ['10','20'],
     *      'class = "A"',
     *      'email like "%gmail.com%"',
     *      'address not like' => '%taipei%',
     *      'gender' => 'Female',
     *      'grade >' => 90,
     * ]
     * 會編譯成
     * WHERE name in ('Alice', 'Bob')
     *   AND age not in ('10','20')
     *   AND class = "A"
     *   AND email like "%gmail.com%"
     *   AND address not like '%taipei%'
     *   AND gender = 'Female'
     *   AND grade > 90
     */
    public function where($where = [], $value=null)
    {
        if(is_string($where) && $value!=null){
            parent::where($where, $value);
        }
        if (is_array($where) && $where != []) {
            foreach ($where as $key => $value) {
                if (is_array($value) && $value != []) {
                    // where in/ where not in
                    if (strpos($key, '!=') === FALSE) {
                        // 'name' => ['Alice','Bob'],
                        $this->builder()->whereIn($key, $value);
                    } else {
                        // 'Age !='  => ['10','20'],
                        $key = implode('', explode('!=', $key));
                        $this->builder()->whereNotIn($key, $value);
                    }
                } elseif ((is_string($value) || is_numeric($value))) {
                    if (is_numeric($key)) {
                        if($value=='')continue;
                        // 'class = A',
                        // 'email like %gmail.com%',
                        $this->builder()->where($value);
                    } elseif (strpos(strtolower($key), 'like') != FALSE) {
                        // 'address not like' => '%taipei%',
                        $str = "$key '$value'";
                        $this->builder()->where($str);
                    } else {
                        // 'gender' => 'Female',
                        // 'grade >' => 90,
                        $this->builder()->where($key, $value);
                    }
                }
            }
        }
        return $this;
    }

    /**
     * Group By函式，支援key陣列或字串傳入
     * EX:
     *      $group = ['column1', 'column2']
     *      $group = 'column1, column2'
     *
     * @param string|array $group
     * @return object $this
     */
    public function group($group)
    {
        if (is_array($group)) {
            foreach ($group as $key) {
                if ($key != '') {
                    $this->builder()->groupBy($key);
                }
            }
        } elseif (is_string($group)) {
            $this->builder()->groupBy($group);
        }

        return $this;
    }

    /**
     * 排序函式，支援key陣列或字串傳入
     * EX: 
     *      $order = ['count' => 'ASC', 's_sn'=>'DESC']
     *      $order = 'count ASC, s_sn DESC';
     *      $order = ['count ASC', 's_sn DESC'];
     *
     * @param string|array $order
     * @return object $this
     */
    public function order($order)
    {
        if (is_string($order)) {
            $order = explode(',', $order);
        }
        foreach ($order as $key => $value) {
            if (is_int($key) && is_string($value) && $value != '') {
                $this->builder()->orderBy($value, 'ASC');
            } elseif (is_string($key) && $key != '' && in_array(strtoupper($value), ['ASC', 'DESC'])) {
                $this->builder()->orderBy($key, strtoupper($value));
            }
        }

        return $this;
    }

    /**
     * limit function
     * @var array|string|int $limit
     * 
     * 傳入 limit(10) => 回傳10筆結果 
     * 傳入 limit("10") => 回傳10筆結果 
     * 傳入 limit("10,20") => 回傳第21~30筆結果
     * 傳入 limit([10,20]) => 回傳第21~30筆結果
     * 
     * @return object $this
     */
    public function limit($limit)
    {
        if (is_string($limit)) {
            $limit = explode(',', $limit);
        }
        if (is_array($limit)) {
            if (count($limit) == 1) {
                $this->builder()->limit($limit[0]);
            } elseif (count($limit) == 2) {
                $this->builder()->limit($limit[0], $limit[1]);
            }
        } elseif (is_integer($limit)) {
            $this->builder()->limit($limit);
        }

        return $this;
    }

    /**
     * 設定是否可取得 rec_status = 'R' 的資料
     *
     * @param  bool $bool
     * @return object $this
     */
    public function onlyGetEnableData($bool)
    {
        $this->getEnableData = $bool;

        return $this;
    }

    /**
     * 回傳查詢結果
     * 
     * @var boolean $isRow = false  預設回傳所有查詢結果
     * @var boolean|null isObject = null 回傳型態(預設讀取$this->returnType) 
     */
    public function get($isRow = false, $isObject = NULL)
    {
        if ($isObject == NULL) {
            $isObject = $this->returnType == 'object';
        }

        if($isObject){
            $this->asObject();
        }else{
            $this->asArray();
        }

        if ($isRow) {
            $data = $this->first();
        } else {
            $data = $this->find();
        }

        return $data;
    }

    /**
     * 更新資料，請配合where來限縮範圍
     *
     * @param array $data 要更新的資料
     * @return boolean 更新成功與否
     */
    public function updateData($data)
    {
        return $this->set($data)->update();
    }

    public function fieldExists($field)
    {
        if(!is_string($field) || $field==''){
            throw new \Exception("Field should be a none empty string when calling fieldExists on Model");
        }

        return $this->db()->fieldExists($field, $this->table);
    }

    /**
     * 設定 c_user, rec_status
     * 參數定義請見 https://codeigniter.tw/user_guide/models/model.html#id21
     *
     * @param array $data
     * @return array
     */
    protected function set_c_user($data)
    {
        // 如果有c_user欄位 => 設定建立者為當前用戶
        if($this->c_userField!='' && !isset($data['data'][$this->c_userField])){
            $c_user = $this->getUserId();
            if ($this->fieldExists($this->c_userField) && $c_user!='') {
                $data['data'][$this->c_userField] = $c_user;
            }
        }

        // 如果有 rec_status 欄位 => 設定為 A
        if($this->recStatusField!='' && !isset($data['data'][$this->recStatusField])) {
            if ($this->fieldExists($this->recStatusField)) {
                $data['data'][$this->recStatusField] = $this->recStatusTrueValue;
            }
        }

        // 如果有設定自動更新 u_time 欄位
        if ($this->updatedField != '' && $this->useTimestamps && !isset($data['data'][$this->updatedField])) {
            // 且 DB 存在該欄位 => 設定為 NULL
            if ($this->fieldExists($this->updatedField)) {
                $data['data'][$this->updatedField] = NULL;
            }
        }


        return $data;
    }

    /**
     * 設定 u_user
     * 參數定義請見 https://codeigniter.tw/user_guide/models/model.html#id21
     *
     * @param array $data
     * @return array
     */
    protected function set_u_user($data)
    {
        if ($this->u_userField!='' && !isset($data['data'][$this->u_userField])) {
            // 如果有 u_user 欄位 => 設定更新為當前用戶
            $userid = $this->getUserId();
            if ($this->fieldExists($this->u_userField) && $userid!='') {
                $data['data'][$this->u_userField] = $userid;
            }
        }

        return $data;
    }

    /**
     * 取得欲刪除的資料pk，供afterDelete使用
     *
     * @param array $data
     * @return void
     */
    protected function get_deleting_ids($data)
    {
        // 以下來自CI操作手冊
        // $data = [
        //     'id' = 正在刪除的pk。
        //     'purge' = 是否是硬刪除(true=硬刪除 false=軟刪除)
        // ];

        // 不處理硬刪除 / 沒pk
        if($data['purge'] || $this->primaryKey=='')return;

        // 初始化變數
        $this->deleting_ids = [];

        if($data['id']!=null){
            // 有傳id => 加入where條件
            $this->whereIn($this->primaryKey, $data['id']);
        }

        // 如果有 rec_status 欄位，設定要抓rec_status = 'A'
        if ($this->recStatusField!='' && $this->fieldExists($this->recStatusField)) {
            $this->where([$this->recStatusField => $this->recStatusTrueValue]);
        }

        // 檢查軟刪除欄位，避免異動到過去已經刪除過的資料
        if($this->useSoftDeletes && $this->deletedField!='' && $this->fieldExists($this->deletedField)){
            $this->where(["{$this->deletedField} IS NULL"]);
        }

        // 取得SQL語句
        $sql = $this->select($this->primaryKey)
                    ->builder()->getCompiledSelect(false);

        // 建立新的query builder以避免複寫delete要用的where條件
        $datas = $this->db()->query($sql)->getResultArray();

        // 取得要被刪除的pk
        $this->deleting_ids = array_column($datas, $this->primaryKey);
    }

    /**
     * 設定d_user, rec_status
     *
     * @param array $data
     * @return void
     */
    protected function set_rec_status($data){
        // 以下來自CI操作手冊
        // $data = [
        //     'id' = 正在刪除的pk。
        //     'purge' = 是否是硬刪除(true=硬刪除 false=軟刪除)
        //     'result' = 調用delete()的結果
        //     'data' = 未使用。
        // ];

        // 先清空query builder
        $this->builder()->resetQuery();

        // 硬刪除/沒設pk => 不做事
        // 沒刪東西/刪除失敗 => 不做事
        if($data['purge'] || $this->primaryKey=='')return;
        if(empty($this->deleting_ids) || $data['result']==false)return;

        // 初始化更新陣列
        $update = [];

        // 如果有 rec_status 欄位，設定rec_status = 'R'
        if ($this->recStatusField!='' && $this->fieldExists($this->recStatusField)) {
            $update[$this->recStatusField] = $this->recStatusFalseValue;
        }

        // 如果有 d_user 欄位，d_user = 刪除者
        $userid = $this->getUserId();
        if ($this->d_userField!='' && $this->fieldExists($this->d_userField) && $userid!='') {
            $update[$this->d_userField] = $userid;
        }

        // 如果需要更新數值
        if (!empty($update)) {
            $this->where([$this->primaryKey => $this->deleting_ids])
                ->set($update);
            if($this->useSoftDeletes && $this->deletedField!='' && $this->fieldExists($this->deletedField)){
                $this->onlyDeleted();
            }
            $this->allowCallbacks(false)->update();
        }

        // 清空變數
        $this->deleting_ids = [];
    }

    /**
     * 設定rec_status
     *
     * @return void
     */
    protected function addsql_rec_status()
    {
        // 如果有 rec_status 欄位 => 設定為 A
        if ($this->recStatusField!='' && $this->fieldExists($this->recStatusField) && $this->getEnableData) {
            $this->builder()->where(["`{$this->table}`.`{$this->recStatusField}`" => $this->recStatusTrueValue]);
        }
    }
}
