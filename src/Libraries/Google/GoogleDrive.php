<?php
namespace DpMIS\Ci4extension\Libraries\Google;

use Google\Client;
use Google\Service\Drive;

/**
 * Class GoogleDrive
 * @package DpMIS\Ci4extension\Libraries\Google
 * Manage Google Drive
 */
class GoogleDrive
{
    private Drive $driveService;
    private $fileContent;
    private $isGetSuccess;

    public function __construct(string $serviceAccountPath = null)
    {
        if (!isset($serviceAccountPath)) {
            throw new \Exception("GoogleDrive::construct() error : service account path is not set");
        }
        if (!file_exists($serviceAccountPath)) {
            throw new \Exception("GoogleDrive::construct() error : service account file not found");
        }
        $client = new Client();
        $client->addScope(Drive::DRIVE);
        $client->setAccessType('offline');
        $client->setAuthConfig($serviceAccountPath);
        $this->driveService = new Drive($client);
        $this->isGetSuccess = false;
        $this->fileContent = null;
    }

    /**
     * Get file from Google Drive
     * @param string $fileId Google Drive file id
     * @param string|null $mimeType Google MIME type
     * @return object $this
     */
    public function getCloudFile(string $fileId, string $mimeType = null): object
    {
        try {
            $this->isGetSuccess = false;
            $this->fileContent = null;
            if (!isset($mimeType)) {
                $file = $this->driveService->files->get($fileId);
                $mimeType = $file->getMimeType();
            }
            $response = $this->driveService->files->export($fileId, $mimeType, ['alt' => 'media']);
            $this->fileContent = $response->getBody()->getContents();
            $this->isGetSuccess = true;
        } catch (\Exception $e) {
            log_message("error", "Can not get file{fileId} with mimeType {mimeType} while calling GoogleDrive::getFile()");
            log_message("error", "GoogleDrive::getFile() call stack : " . $e->getTraceAsString());
            log_message("error", "GoogleDrive::getFile() error : " . $e->getMessage());
        }

        return $this;
    }

    /**
     * Get file Content which download from Google Drive
     * @return string $fileContent
     */
    public function getContent()
    {
        if($this->isGetSuccess){
            return $this->fileContent;
        }else{
            throw new \Exception("File not get successfully before calling GoogleDrive::getContent()");
        }
    }

    /**
     * Save file Content to local file
     * @param string $filePath Local file path name
     * @return bool
     */
    public function saveAs(string $filePath): bool
    {
        try {
            $dirPath = dirname($filePath);
            if(!is_dir($dirPath)) { 
                mkdir($dirPath, 0775, true);
            }
            file_put_contents($filePath, $this->getContent());
            return true;
        } catch (\Exception $e) {
            log_message("error", "GoogleDrive::saveFile() call stack : " . $e->getTraceAsString());
            log_message("error", "GoogleDrive::saveFile() error : " . $e->getMessage());
            return false;
        }
    }
}