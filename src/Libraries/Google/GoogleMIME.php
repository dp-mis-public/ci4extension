<?php
namespace DpMIS\Ci4extension\Libraries\Google;

// https://developers.google.com/drive/api/guides/ref-export-formats
enum GoogleMIME: string
{
    // Documents
    case MICROSOFT_WORD = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    case OPEN_DOCUMENT_TEXT = "application/vnd.oasis.opendocument.text";
    case RICH_TEXT = "application/rtf";
    case PLAIN_TEXT = "text/plain";

    // Spreadsheets
    case MICROSOFT_EXCEL = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    case OPEN_DOCUMENT_SPREADSHEET = "application/vnd.oasis.opendocument.spreadsheet";
    case CSV = "text/csv";
    case TSV = "text/tab-separated-values";

    // Presentations
    case MICROSOFT_POWERPOINT = "application/vnd.openxmlformats-officedocument.presentationml.presentation";
    case OPEN_DOCUMENT_PRESENTATION = "application/vnd.oasis.opendocument.presentation";
    case POWERPOINT_SHOW = "application/vnd.openxmlformats-officedocument.presentationml.slideshow";

    // Drawings
    case PDF = "application/pdf";
    case JPEG = "image/jpeg";
    case PNG = "image/png";
    case SVG = "image/svg+xml";

    // Audios
    case MP3 = "audio/mpeg";
    case WAV = "audio/wav";
    case FLAC = "audio/flac";
    case OPUS = "audio/opus";

    // Others
    case ZIP = "application/zip";
    case EPUB = "application/epub+zip";
    case JSON = "application/json";
    case XML = "application/xml";
    case WEB_PAGE = "text/html";
    case CSS = "text/css";
    case JAVASCRIPT = "text/javascript";
    case PHP = "application/x-httpd-php";
    case BINARY = "application/octet-stream";
    case FOLDER = "application/vnd.google-apps.folder";
}